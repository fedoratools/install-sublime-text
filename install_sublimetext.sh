#!/bin/bash

# Adaugă repository-ul DNF pentru Sublime Text
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo tee /etc/yum.repos.d/sublime-text.repo <<EOF
[sublime-text]
name=Sublime Text - x86_64
baseurl=https://download.sublimetext.com/rpm/stable/x86_64
enabled=1
gpgcheck=1
gpgkey=https://download.sublimetext.com/sublimehq-rpm-pub.gpg
EOF

# Instalează Sublime Text folosind DNF
sudo dnf install sublime-text

# Deschide Sublime Text
subl

