# Instalare Sublime Text

Acest script Bash simplu automatizează procesul de instalare a Sublime Text pe sistemele care utilizează gestionarul de pachete DNF.

## Utilizare

1. Clonează acest repository:

   ```bash
   git clone https://exemplu.com/repo/script-sublime.git
   ```

2. Navighează în directorul clonat:

   ```bash
   cd script-sublime
   ```

3. Rulează scriptul:

   ```bash
   ./install_sublime.sh
   ```

## Conținut

- `install_sublime.sh`: Scriptul Bash care adaugă depozitul DNF pentru Sublime Text, instalează Sublime Text și deschide aplicația.